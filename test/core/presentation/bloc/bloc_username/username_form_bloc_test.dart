import 'package:appyou/core/presentation/bloc/bloc_username/username_form_bloc.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("username", () {
    blocTest<UsernameBloc, UsernameState>(
      'emits [MyState] when MyEvent is added.',
      build: () => UsernameBloc(),
      act: (cubit) => cubit.add(EventFieldUsernameEvent(username: "tes")),
      expect: () => [
        StateWaitingTextFieldUsername(),
        StateSuccesTextFieldUsername(username: "tes")
      ],
    );
  });
}
