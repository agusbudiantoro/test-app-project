import 'package:appyou/features/auth/domain/entities/entity_user.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:appyou/features/auth/presentation/cubit/bloc_login/login_bloc.dart';

void main() {
  group('login bloc ...', () {
    final bloc = LoginBloc();
    AuthClass auth = AuthClass(username: "kminchelle", password: "0lelplR");
    AuthClass dataResult = AuthClass(
        username: "kminchelle",
        email: "kminchelle@qq.com",
        id: 15,
        firstName: "Jeanne",
        lastName: "Halvorson",
        gender: "female",
        image: "https://robohash.org/autquiaut.png");
    blocTest<LoginBloc, LoginState>(
      'emits [MyState] when MyEvent is added.',
      build: () => bloc,
      act: (cubit) => cubit.login(auth),
      expect: () => [StateWaitingLogin(), StateSuccessLogin(data: dataResult)],
    );
  });
}
