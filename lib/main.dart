import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/auth/presentation/auth_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: MyColors.primaryBackground,
      ),
      home: const LoginScreen(),
    );
  }
}
