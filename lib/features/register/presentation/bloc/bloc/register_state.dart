part of 'register_bloc.dart';

abstract class RegisterState {}

class RegisterInitial extends RegisterState {}
class StateSuccessRegister extends RegisterState {}
class StateWaitingRegister extends RegisterState {}
class StateFailedRegister extends RegisterState {
  String? message;

  StateFailedRegister({this.message});
}
