import 'package:bloc/bloc.dart';

part 'register_state.dart';

class RegisterBloc extends Cubit<RegisterState> {
  RegisterBloc() : super(RegisterInitial());

  void register(){
    emit(StateSuccessRegister());
  }
}
