import 'package:appyou/core/presentation/bloc/bloc_email/email_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_password/password_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_password_confirm/password_confirm_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_require_form/require_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_username/username_form_bloc.dart';
import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/core/utils/widgets/appbar.dart';
import 'package:appyou/features/register/presentation/bloc/bloc/register_bloc.dart';
import 'package:appyou/features/register/presentation/widgets/register_body.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
      return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => RegisterBloc()),
        BlocProvider(create: (context) => UsernameBloc()),
        BlocProvider(create: (context) => EmailBloc()),
        BlocProvider(create: (context) => PasswordFormBloc()),
        BlocProvider(create: (context) => PasswordConfirmFormBloc()),
        BlocProvider(create: (context) => RequireFormBloc()),
      ],
      child: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                MyColors.primaryBackground,
                MyColors.primaryBackground2,
                MyColors.primaryBackground3,
              ],
            )
          ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: SafeArea(
            child: Container(
              color: Colors.transparent,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: [
                    AppbarCustom(clickCallBack: (val){
                      Navigator.pop(context);
                    }),
                    RegisterBody(),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 22),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Have an account? ", style: customTextStyle(weight: FontWeight.w500, size: 13),),
                            GestureDetector(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: Text("Login here", style: customTextStyleGradient(weight: FontWeight.w500, size: 13),)),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
