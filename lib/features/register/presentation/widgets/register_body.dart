import 'package:appyou/core/presentation/bloc/bloc_email/email_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_password/password_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_password_confirm/password_confirm_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_require_form/require_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_username/username_form_bloc.dart';
import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/core/utils/widgets/button.dart';
import 'package:appyou/core/utils/widgets/form.dart';
import 'package:appyou/features/register/presentation/bloc/bloc/register_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterBody extends StatelessWidget {
  RegisterBody({Key? key}) : super(key: key);
  final UsernameBloc blocUsername = UsernameBloc();
  final EmailBloc blocEmail = EmailBloc();
  final PasswordFormBloc blocPassword = PasswordFormBloc();
  final PasswordConfirmFormBloc blocPasswordConfirm = PasswordConfirmFormBloc();
  final RegisterBloc blocRegist = RegisterBloc();
  final RequireFormBloc blocRequireForm = RequireFormBloc();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 1.5,
      width: size.width,
      color: Colors.transparent,
      child: Padding(
        padding: EdgeInsets.only(
            left: 8.0, right: 8.0, top: size.height / 10, bottom: 8.0),
        child: Column(
          children: [
            Container(
                padding:
                    EdgeInsets.symmetric(horizontal: (10 * size.width) / 100),
                alignment: Alignment.centerLeft,
                child: Text(
                  "Register",
                  style: customTextStyle(
                      size: 24,
                      weight: FontWeight.w700,
                      colors: MyColors.fontTitle),
                )),
            SizedBox(
              height: (6.3 * size.width) / 100,
            ),
            Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 23),
                child: customTextField(
                    height: size.height / 15,
                    width: size.width,
                    clickCallback: (val) {
                      blocEmail.add(EventFieldEmailEvent(email: val));
                    },
                    myHint: 'Enter Email')),
            Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 23),
                child: customTextField(
                    height: size.height / 15,
                    width: size.width,
                    clickCallback: (val) {
                      blocUsername.add(EventFieldUsernameEvent(username: val));
                    },
                    myHint: 'Create Username')),
            Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 23),
                child: customTextField(
                    height: size.height / 15,
                    width: size.width,
                    myHint: 'Create Password',
                    clickCallback: (val) {
                      blocPassword.add(EventFieldPasswordEvent(password: val));
                    },
                    suffixIcon: Icons.remove_red_eye)),
            Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 23),
                child: customTextField(
                    height: size.height / 15,
                    width: size.width,
                    clickCallback: (val) {
                      blocPasswordConfirm
                          .add(EventFieldPasswordConfirmEvent(password: val));
                    },
                    myHint: 'Confirm Password',
                    suffixIcon: Icons.remove_red_eye)),
            BlocBuilder<EmailBloc, EmailState>(
              bloc: blocEmail,
              builder: (context, state) {
                if (state is StateSuccesTextFieldEmail) {
                  return BlocBuilder<UsernameBloc, UsernameState>(
                    bloc: blocUsername,
                    builder: (context, stateUsername) {
                      if (stateUsername is StateSuccesTextFieldUsername) {
                        return BlocBuilder<PasswordFormBloc, PasswordFormState>(
                          bloc: blocPassword,
                          builder: (context, statePass) {
                            if (statePass is StateSuccesTextFieldPassword) {
                              return BlocBuilder<PasswordConfirmFormBloc,
                                  PasswordConfirmFormState>(
                                bloc: blocPasswordConfirm,
                                builder: (context, statePassConfirm) {
                                  if (statePassConfirm
                                      is StateSuccesTextFieldPasswordConfirm) {
                                        blocRequireForm.add(RequireFormEventRegister(
                                      username: stateUsername.username,
                                      password: statePass.password,
                                      email: state.email,
                                      passwordConfirm: statePassConfirm.password
                                      ));
                                    return BlocBuilder<RequireFormBloc,
                                        RequireFormState>(
                                          bloc: blocRequireForm,
                                      builder: (context, stateReq) {
                                        if(stateReq is RequireFormSuccess){
                                          return Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 23),
                                          child: customButton(
                                              clickCallback: () {
                                                if(stateReq.status!){
                                                  blocRegist.register();
                                                }
                                              },
                                              height: size.height / 15,
                                              width: size.width,
                                              textTitle: "Register",
                                              status: stateReq.status!),
                                        );
                                        }
                                        return Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 23),
                                          child: customButton(
                                              clickCallback: () {
                                                
                                              },
                                              height: size.height / 15,
                                              width: size.width,
                                              textTitle: "Register",
                                              status: false),
                                        );
                                      },
                                    );
                                  }
                                  return Container();
                                },
                              );
                            }
                            return Container();
                          },
                        );
                      }
                      return Container();
                    },
                  );
                }
                return Container();
              },
            )
          ],
        ),
      ),
    );
  }

  bool cekStat(String email, String usernam, String pass, String passConfirm) {
    if (email.isNotEmpty &&
        usernam.isNotEmpty &&
        pass.isNotEmpty &&
        passConfirm.isNotEmpty) {
      if (pass == passConfirm) {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }
}
