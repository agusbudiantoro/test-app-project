import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class AuthClass extends Equatable {
  int? id;
  String? username;
  String? password;
  String? email;
  String? firstName;
  String? lastName;
  String? gender;
  String? image;

  AuthClass(
      {this.id,
      this.username,
      this.email,
      this.password,
      this.firstName,
      this.lastName,
      this.gender,
      this.image});

  @override
  List<Object> get props =>
      [id!, username!, firstName!, email!, lastName!, gender!, image!];

  AuthClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    password = json['password'];
    email = json['email'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    gender = json['gender'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['username'] = username;
    data['password'] = password;
    data['email'] = email;
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['gender'] = gender;
    data['image'] = image;
    return data;
  }
}
