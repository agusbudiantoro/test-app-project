part of 'obsecure_text_cubit.dart';

abstract class ObsecureTextState {}

class ObsecureTextInitial extends ObsecureTextState {}
class ObsecureTextSuccess extends ObsecureTextState {
  bool? status;
  ObsecureTextSuccess({this.status});
}
class ObsecureTextFailed extends ObsecureTextState {}
