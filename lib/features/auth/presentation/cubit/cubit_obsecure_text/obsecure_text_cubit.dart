import 'package:bloc/bloc.dart';

part 'obsecure_text_state.dart';

class ObsecureTextCubit extends Cubit<ObsecureTextState> {
  ObsecureTextCubit() : super(ObsecureTextInitial());

  void changeStatus(bool stat) async{
    try {
      emit(ObsecureTextSuccess(status: !stat));
    } catch (e) {
      emit(ObsecureTextFailed());
    }
  }
}
