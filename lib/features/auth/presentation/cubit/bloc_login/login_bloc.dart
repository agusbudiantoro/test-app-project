import 'dart:convert';

import 'package:appyou/features/auth/data/datasource/login_api.dart';
import 'package:appyou/features/auth/domain/entities/entity_user.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'login_state.dart';

class LoginBloc extends Cubit<LoginState> {
  LoginBloc() : super(LoginInitial());

  void login(AuthClass data) async {
    emit(StateWaitingLogin());
    try {
      dynamic value = await LoginApi.loginAkun(data);
      var hasil = jsonDecode(value);
      AuthClass hasilConvert = AuthClass.fromJson(hasil);
      emit(StateSuccessLogin(data: hasilConvert));
    } catch (e) {
      emit(StateFailedLogin(message: e.toString()));
    }
  }
}
