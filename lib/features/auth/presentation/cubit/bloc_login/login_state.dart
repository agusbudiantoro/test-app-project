part of 'login_bloc.dart';

@immutable
abstract class LoginState extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class StateSuccessLogin extends LoginState {
  final AuthClass? data;
  StateSuccessLogin({this.data});
  @override
  List<AuthClass> get props => [data!];
}

class StateWaitingLogin extends LoginState {
  @override
  List<Object> get props => [];
}

class StateFailedLogin extends LoginState {
  final String? message;

  StateFailedLogin({this.message});
}
