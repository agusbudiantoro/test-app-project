import 'package:appyou/core/presentation/bloc/bloc_password/password_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_require_form/require_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_username/username_form_bloc.dart';
import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/core/utils/widgets/appbar.dart';
import 'package:appyou/features/auth/presentation/cubit/bloc_login/login_bloc.dart';
import 'package:appyou/features/auth/presentation/cubit/cubit_obsecure_text/obsecure_text_cubit.dart';
import 'package:appyou/features/auth/presentation/widgets/login_body.dart';
import 'package:appyou/features/register/presentation/register_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => LoginBloc()),
        BlocProvider(create: (context) => UsernameBloc()),
        BlocProvider(create: (context) => PasswordFormBloc()),
        BlocProvider(create: (context) => RequireFormBloc()),
        BlocProvider(create: (context) => ObsecureTextCubit()),
      ],
      child: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            MyColors.primaryBackground,
            MyColors.primaryBackground2,
            MyColors.primaryBackground3,
          ],
        )),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: SafeArea(
            child: Container(
              color: Colors.transparent,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: [
                    AppbarCustom(clickCallBack: (val){
                      Navigator.pop(context);
                    }),
                    LoginBody(),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 22),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "No account? ",
                              style: customTextStyle(
                                  weight: FontWeight.w500, size: 13),
                            ),
                            GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => const RegisterScreen()));
                                },
                                child: Text(
                                  "Register here",
                                  style: customTextStyleGradient(
                                      weight: FontWeight.w500, size: 13),
                                )),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
