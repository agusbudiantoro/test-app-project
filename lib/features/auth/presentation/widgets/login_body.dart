import 'package:appyou/core/presentation/bloc/bloc_password/password_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_require_form/require_form_bloc.dart';
import 'package:appyou/core/presentation/bloc/bloc_username/username_form_bloc.dart';
import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/core/utils/widgets/button.dart';
import 'package:appyou/core/utils/widgets/form.dart';
import 'package:appyou/features/auth/domain/entities/entity_user.dart';
import 'package:appyou/features/auth/presentation/cubit/bloc_login/login_bloc.dart';
import 'package:appyou/features/auth/presentation/cubit/cubit_obsecure_text/obsecure_text_cubit.dart';
import 'package:appyou/features/home/presentation/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginBody extends StatelessWidget {
  LoginBody({
    Key? key,
  }) : super(key: key);
  final UsernameBloc blocUsername = UsernameBloc();
  final PasswordFormBloc blocPassword = PasswordFormBloc();
  final LoginBloc blocLogin = LoginBloc();
  final ObsecureTextCubit cubitObsText = ObsecureTextCubit();
  final RequireFormBloc blocRequireForm = RequireFormBloc();
  final TextEditingController username = TextEditingController();
  final TextEditingController password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 2,
      width: size.width,
      color: Colors.transparent,
      child: Padding(
          padding: EdgeInsets.only(
              left: 8.0, right: 8.0, top: size.height / 10, bottom: 8.0),
          child: mycol(size, context)),
    );
  }

  Column mycol(Size size, BuildContext context) {
    return Column(
      children: [
        Container(
            padding: EdgeInsets.symmetric(horizontal: (10 * size.width) / 100),
            alignment: Alignment.centerLeft,
            child: Text(
              "Login",
              style: customTextStyle(
                  size: 24,
                  weight: FontWeight.w700,
                  colors: MyColors.fontTitle),
            )),
        SizedBox(
          height: (6.3 * size.width) / 100,
        ),
        Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 23),
            child: customTextField(
              height: size.height / 15,
              width: size.width,
              clickCallback: (val) {
                blocUsername.add(EventFieldUsernameEvent(username: val));
              },
              myHint: "Enter Username/Email",
            )),
        Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 23),
            child: BlocBuilder<ObsecureTextCubit, ObsecureTextState>(
              bloc: cubitObsText,
              builder: (context, state) {
                if (state is ObsecureTextSuccess) {
                  return customTextField(
                      obs: state.status,
                      buttonIcon: (val) {
                        cubitObsText.changeStatus(val);
                      },
                      height: size.height / 15,
                      clickCallback: (val) {
                        blocPassword
                            .add(EventFieldPasswordEvent(password: val));
                      },
                      width: size.width,
                      myHint: "password",
                      suffixIcon: Icons.remove_red_eye);
                }
                return customTextField(
                    obs: true,
                    height: size.height / 15,
                    clickCallback: (val) {
                      blocPassword.add(EventFieldPasswordEvent(password: val));
                    },
                    buttonIcon: (val) {
                      cubitObsText.changeStatus(val);
                    },
                    width: size.width,
                    myHint: "password",
                    suffixIcon: Icons.remove_red_eye);
              },
            )),
        BlocBuilder<UsernameBloc, UsernameState>(
          bloc: blocUsername,
          builder: (context, stateUser) {
            if (stateUser is StateSuccesTextFieldUsername) {
              return BlocBuilder<PasswordFormBloc, PasswordFormState>(
                bloc: blocPassword,
                builder: (context, state) {
                  if (state is StateSuccesTextFieldPassword) {
                    blocRequireForm.add(RequireFormEventLogin(
                        username: stateUser.username,
                        password: state.password));
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 23),
                      child: BlocBuilder<RequireFormBloc, RequireFormState>(
                        bloc: blocRequireForm,
                        builder: (context, stateForm) {
                          if (stateForm is RequireFormSuccess) {
                            return BlocListener<LoginBloc, LoginState>(
                              bloc: blocLogin,
                              listener: (context, state) {
                                if (state is StateWaitingLogin) {
                                  final snackBar = SnackBar(
                                    backgroundColor: Colors.black,
                                    content: Text("Loading", style: customTextStyleGradient(colors: MyColors.fontTitle, size: 14, weight: FontWeight.w600),),
                                  );
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);
                                }
                                if(state is StateFailedLogin){
                                  final snackBar = SnackBar(
                                    backgroundColor: Colors.black,
                                    content: Text(state.message.toString(), style: customTextStyleGradient(colors: MyColors.fontTitle, size: 14, weight: FontWeight.w600),),
                                  );
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);
                                }
                                if (state is StateSuccessLogin) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HomePage()));
                                }
                              },
                              child: customButton(
                                  clickCallback: () {
                                    if (stateForm.status!) {
                                      AuthClass dataLogin = AuthClass(
                                          username: stateUser.username,
                                          password: state.password);
                                      blocLogin.login(dataLogin);
                                    }
                                  },
                                  height: size.height / 15,
                                  width: size.width,
                                  textTitle: "Login",
                                  status: stateForm.status!),
                            );
                          }
                          return customButton(
                              clickCallback: () {},
                              height: size.height / 15,
                              width: size.width,
                              textTitle: "Login",
                              status: false);
                        },
                      ),
                    );
                  }
                  return Container();
                },
              );
            }
            return Container();
          },
        ),
      ],
    );
  }

  bool cekStat(String usernam, String pass) {
    if (usernam.isNotEmpty && pass.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }
}
