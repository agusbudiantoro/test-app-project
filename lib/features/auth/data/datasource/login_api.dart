import 'dart:io';

import 'package:appyou/core/domain/baseurl.dart';
import 'package:appyou/features/auth/domain/entities/entity_user.dart';
import 'package:http/http.dart';
import 'package:http/io_client.dart';

class LoginApi {
static Future<dynamic> loginAkun(data)async{
    HttpClient client = HttpClient();
    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    IOClient ioClient = IOClient(client);
    AuthClass convertData = data;
    Map<String, String>body={
      'username':convertData.username.toString(), //"kminchelle"
      'password':convertData.password.toString() //"0lelplR"
    };
    try {
      Response response = await ioClient.post(Uri.parse('${baseUrl}login'), body: body);
      if(response.statusCode == 200){
        return response.body;
      }
      else{
        return throw Exception(response.body.toString());
      }
    } catch (e) {
      return throw Exception(e.toString());
    }
  }
}