import 'dart:io';

class ProfileModel {
  String? valueGender;
  String? name;
  String? birthday;
  String? hero;
  String? noneField;
  String? heightBody;
  String? weightBody;
  String? age;
  DateTime? ageFormatDate;
  File? path;

  ProfileModel({this.ageFormatDate,this.age,this.birthday, this.heightBody, this.hero, this.name, this.noneField, this.path, this.valueGender, this.weightBody});
}