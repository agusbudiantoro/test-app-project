import 'package:appyou/core/presentation/widgets/appbar_general_custom.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/home/presentation/bloc/bloc_tombol_edit_form_about/tombol_edit_form_about_bloc.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_file_gambar/upload_file_gambar_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_input_date/input_date_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_input_gender/input_data_gender_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/saved_changes/saved_changes_cubit.dart';
import 'package:appyou/features/home/presentation/widgets/header.dart';
import 'package:appyou/features/home/presentation/widgets/home_body.dart';
import 'package:appyou/features/interests/presentation/cubit/cubit_add_interest/add_interest_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  final UploadFileGambarCubit cubitBlocUpload = UploadFileGambarCubit();
  final SavedChangesCubit cubitBlocSavedData = SavedChangesCubit();
  final InputDataGenderCubit cubitBlocChangeGender = InputDataGenderCubit();
  final InputDateCubit cubitBlocChangeDate = InputDateCubit();
  final AddInterestCubit cubitAddSaveInterest = AddInterestCubit();
  final AddInterestCubit cubitAddInterest = AddInterestCubit();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => TombolEditFormAboutBloc()),
          BlocProvider(create: (context) => UploadFileGambarCubit()),
          BlocProvider(create: (context) => SavedChangesCubit()),
          BlocProvider(create: (context) => InputDataGenderCubit()),
          BlocProvider(create: (context) => InputDateCubit()),
          BlocProvider(create: (context) => AddInterestCubit()),
        ],
        child: Container(
          color: MyColors.primaryBackground3,
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            body: SafeArea(
              child: Container(
                color: Colors.transparent,
                child: Column(
                  children: [
                    Expanded(
                        flex: 1,
                        child: BlocBuilder<SavedChangesCubit, SavedChangesState>(
                          bloc: cubitBlocSavedData,
                          builder: (context, state) {
                            if(state is SavedChangeProfileSuccess){
                              return AppbarCustomGeneral(clickCallBack: (val) {
                              Navigator.pop(context);
                            },name: state.dataProfil?.name,);
                            }
                            return AppbarCustomGeneral(clickCallBack: (val) {
                              Navigator.pop(context);
                            },name: "@johndoe",);
                          },
                        )),
                    Expanded(
                      flex: 12,
                      child: LayoutBuilder(builder:
                          (BuildContext context, BoxConstraints constraints) {
                        final Size biggest = constraints.biggest;
                        return SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            physics: const ClampingScrollPhysics(),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: Column(
                                children: [
                                  BlocBuilder<UploadFileGambarCubit,
                                      UploadFileGambarState>(
                                    bloc: cubitBlocUpload,
                                    builder: (context, state) {
                                      if (state is UploadFileGambarSuccess) {
                                        return HeaderWidget(
                                          biggest: biggest,
                                          file: state.file!,
                                          cubitSavedData: cubitBlocSavedData,
                                        );
                                      }
                                      return HeaderWidget(
                                          biggest: biggest,
                                          cubitSavedData: cubitBlocSavedData);
                                    },
                                  ),
                                  SizedBox(
                                    height: biggest.height / 30,
                                  ),
                                  BodyWidgetHome(
                                      biggest: biggest,
                                      blocUpload: cubitBlocUpload,
                                      cubitBlocSavedData: cubitBlocSavedData,
                                      cubitBlocChangeDate: cubitBlocChangeDate,
                                      cubitBlocChangeGender:
                                          cubitBlocChangeGender,
                                          cubitAddSaveInterest: cubitAddSaveInterest, cubitAddInterest: cubitAddInterest),
                                  SizedBox(
                                    height: biggest.height / 3,
                                  )
                                ],
                              ),
                            ));
                      }),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
