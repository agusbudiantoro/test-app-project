import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/home/domain/models/model_profile.dart';
import 'package:appyou/features/home/presentation/bloc/bloc_tombol_edit_form_about/tombol_edit_form_about_bloc.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_file_gambar/upload_file_gambar_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_input_date/input_date_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_input_gender/input_data_gender_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/saved_changes/saved_changes_cubit.dart';
import 'package:appyou/features/home/presentation/utils/form_about.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WidgetAbout extends StatefulWidget {
  const WidgetAbout({
    Key? key,
    required this.biggest,
    required this.blocFormEdit,
    required this.status,
    required this.blocUpload,
    required this.cubitBlocSavedData,
    required this.cubitBlocChangeGender,
    required this.cubitBlocChangeDate,
  }) : super(key: key);

  final Size biggest;
  final bool status;
  final TombolEditFormAboutBloc blocFormEdit;
  final UploadFileGambarCubit blocUpload;
  final SavedChangesCubit cubitBlocSavedData;
  final InputDataGenderCubit cubitBlocChangeGender;
  final InputDateCubit cubitBlocChangeDate;

  @override
  State<WidgetAbout> createState() => _WidgetAboutState();
}

class _WidgetAboutState extends State<WidgetAbout> {
  String? valueGender = "";
  String? name = "";
  String? birthday = "";
  String? hero = "";
  String? noneField = "";
  String? heightBody = "";
  String? weightBody = "";
  DateTime? age = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Container(
        width: widget.biggest.width,
        decoration: const BoxDecoration(
            color: MyColors.card1,
            borderRadius: BorderRadius.all(Radius.circular(16))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: 27, top: widget.biggest.height / 50),
                  child: Text(
                    "About",
                    style: customTextStyle(
                        size: 14,
                        weight: FontWeight.w700,
                        colors: MyColors.fontTitle),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(
                        right: 8, top: widget.biggest.height / 50),
                    child: (widget.status)
                        ? GestureDetector(
                            onTap: () {
                              ProfileModel modelProfil = ProfileModel(
                                  birthday: birthday,
                                  heightBody: heightBody,
                                  ageFormatDate: age,
                                  hero: hero,
                                  name: name,
                                  noneField: noneField,
                                  valueGender: valueGender,
                                  weightBody: weightBody);
                              widget.cubitBlocSavedData.simpanData(modelProfil);
                              widget.blocFormEdit
                                  .add(ChangeEditFormAboutEvent(status: false));
                            },
                            child: Text(
                              "Save & Update",
                              style: customTextStyleGradient(
                                  weight: FontWeight.w500, size: 13),
                            ))
                        : IconButton(
                            onPressed: () {
                              widget.blocFormEdit
                                  .add(ChangeEditFormAboutEvent(status: true));
                            },
                            icon: const Icon(
                              Icons.mode_edit_outline_outlined,
                              color: Colors.white,
                              size: 18.75,
                            )))
              ],
            ),
            (widget.status)
                ? WidgetFormAbout(
                    biggest: widget.biggest,
                    cubitBlocUpload: widget.blocUpload,
                    cubitBlocSavedData: widget.cubitBlocSavedData,
                    cubitBlocChangeDate: widget.cubitBlocChangeDate,
                    cubitBlocChangeGender: widget.cubitBlocChangeGender,
                    name: (val) {
                      setState(() {
                        name = val;
                      });
                    },
                    birthday: (val,date) {
                      setState(() {
                        birthday = val;
                        age = date;
                      });
                    },
                    heightBody: (val) {
                      setState(() {
                        heightBody = val;
                      });
                    },
                    hero: (val) {
                      setState(() {
                        hero = val;
                      });
                    },
                    noneField: (val) {
                      setState(() {
                        noneField = val;
                      });
                    },
                    valueGender: (val) {
                      setState(() {
                        valueGender = val;
                      });
                    },
                    weightBody: (val) {
                      setState(() {
                        weightBody = val;
                      });
                    },
                  )
                : Padding(
                    padding: EdgeInsets.only(
                        left: 27, bottom: 17, right: widget.biggest.width / 18),
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child:
                              Wrap(alignment: WrapAlignment.start, children: [
                            BlocBuilder<SavedChangesCubit, SavedChangesState>(
                              bloc: widget.cubitBlocSavedData,
                              builder: (context, state) {
                                if (state is SavedChangeProfileSuccess) {
                                  return Column(
                                    children: [
                                      rowAbout('${state.dataProfil!.birthday} (Age ${state.dataProfil?.age})', "Birthday: "),
                                      rowAbout(state.dataProfil!.hero!, "Heroscope: "),
                                      rowAbout(state.dataProfil!.noneField!, "none: "),
                                      rowAbout(state.dataProfil!.heightBody!, "Height: "),
                                      rowAbout(state.dataProfil!.weightBody!, "Weight: "),
                                    ],
                                  );
                                }
                                return Text(
                                  "Add in your your to help others know you better your to help others know you better lorem ipsum",
                                  style: customTextStyle(
                                      size: 14,
                                      weight: FontWeight.w500,
                                      colors: MyColors.fontTitle),
                                );
                              },
                            ),
                          ]),
                        ),
                      ],
                    ),
                  )
          ],
        ));
  }

  Widget rowAbout(String state, String label) {
    return Container(
      margin: const EdgeInsets.only(bottom: 15),
      child: Row(
        children: [
          Text(
            label,
            style: customTextStyle(
                size: 13, weight: FontWeight.w500, colors: MyColors.hintText),
          ),
          Text(state,
              style: customTextStyle(
                  size: 13, weight: FontWeight.w500, colors: MyColors.fontTitle)),
        ],
      ),
    );
  }
}
