import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/home/presentation/bloc/bloc_tombol_edit_form_about/tombol_edit_form_about_bloc.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_file_gambar/upload_file_gambar_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_input_date/input_date_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_input_gender/input_data_gender_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/saved_changes/saved_changes_cubit.dart';
import 'package:appyou/features/home/presentation/widgets/chips_interest.dart';
import 'package:appyou/features/home/presentation/widgets/widget_about.dart';
import 'package:appyou/features/interests/presentation/cubit/cubit_add_interest/add_interest_cubit.dart';
import 'package:appyou/features/interests/presentation/interest_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BodyWidgetHome extends StatelessWidget {
  BodyWidgetHome(
      {super.key,
      required this.biggest,
      required this.blocUpload,
      required this.cubitBlocSavedData,
      required this.cubitBlocChangeDate,
      required this.cubitBlocChangeGender,
      required this.cubitAddSaveInterest,
      required this.cubitAddInterest});
  final UploadFileGambarCubit blocUpload;
  final SavedChangesCubit cubitBlocSavedData;
  final Size biggest;
  final TombolEditFormAboutBloc blocFormEdit = TombolEditFormAboutBloc();
  final InputDataGenderCubit cubitBlocChangeGender;
  final InputDateCubit cubitBlocChangeDate;
  final AddInterestCubit cubitAddSaveInterest;
  final AddInterestCubit cubitAddInterest;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder<TombolEditFormAboutBloc, TombolEditFormAboutState>(
          bloc: blocFormEdit,
          builder: (context, state) {
            if (state is TombolEditFormAboutSuccess) {
              return WidgetAbout(
                biggest: biggest,
                blocFormEdit: blocFormEdit,
                status: state.status!,
                blocUpload: blocUpload,
                cubitBlocSavedData: cubitBlocSavedData,
                cubitBlocChangeDate: cubitBlocChangeDate,
                cubitBlocChangeGender: cubitBlocChangeGender,
              );
            }
            return WidgetAbout(
                biggest: biggest,
                blocFormEdit: blocFormEdit,
                status: false,
                blocUpload: blocUpload,
                cubitBlocSavedData: cubitBlocSavedData,
                cubitBlocChangeDate: cubitBlocChangeDate,
                cubitBlocChangeGender: cubitBlocChangeGender);
          },
        ),
        SizedBox(
          height: biggest.height / 40,
        ),
        Container(
            width: biggest.width,
            decoration: const BoxDecoration(
                color: MyColors.card1,
                borderRadius: BorderRadius.all(Radius.circular(16))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.only(left: 27, top: biggest.height / 50),
                      child: Text(
                        "Interest",
                        style: customTextStyle(
                            size: 14,
                            weight: FontWeight.w700,
                            colors: MyColors.fontTitle),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(right: 8, top: biggest.height / 50),
                      child: IconButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => InterestScreen(
                                      cubitAddInterest: cubitAddInterest,
                                      cubitAddSaveInterest:
                                          cubitAddSaveInterest,
                                    )));
                          },
                          icon: const Icon(
                            Icons.mode_edit_outline_outlined,
                            color: Colors.white,
                            size: 18.75,
                          )),
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 27, bottom: 17, right: biggest.width / 18),
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: Wrap(alignment: WrapAlignment.start, children: [
                          BlocBuilder<AddInterestCubit, AddInterestState>(
                            bloc: cubitAddSaveInterest,
                            builder: (context, state) {
                              if(state is AddSaveInterestSuccess){
                                return Container(
                                  child: chipsDynamisInterest(state.interest!),
                                );
                              }
                              return Text(
                                "Add in your interest to find a better match",
                                style: customTextStyle(
                                    size: 14,
                                    weight: FontWeight.w500,
                                    colors: MyColors.fontTitle),
                              );
                            },
                          ),
                        ]),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ],
    );
  }
}
