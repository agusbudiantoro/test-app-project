import 'dart:io';
import 'dart:ui';

import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/home/domain/models/model_profile.dart';
import 'package:appyou/features/home/presentation/bloc/saved_changes/saved_changes_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({Key? key, required this.biggest, this.file,required this.cubitSavedData})
      : super(key: key);
  final Size biggest;
  final File? file;
  final SavedChangesCubit? cubitSavedData;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: biggest.height / 3.7,
        width: biggest.width,
        decoration: (file != null) ? decoBoxImage() : decoBox(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            BlocBuilder<SavedChangesCubit, SavedChangesState>(
              bloc: cubitSavedData,
              builder: (context, state) {
                  if(state is SavedChangeProfileSuccess){
                    return headerDynamis(state.dataProfil!);
                  }
                return headerStatic();
              },
            )
            
          ],
        ));
  }

  Widget headerStatic(){
    return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 13, bottom: 17),
                  child: Text(
                    "@johndoe123,",
                    style: customTextStyle(
                        size: 16,
                        weight: FontWeight.w700,
                        colors: MyColors.fontTitle),
                  ),
                )
              ],
            );
  }

  Widget headerDynamis(ProfileModel data){
    return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 13, bottom: 6),
                      child: Text(
                        '${data.name}, ',
                        style: customTextStyle(
                            size: 16,
                            weight: FontWeight.w700,
                            colors: MyColors.fontTitle),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 5, bottom: 6),
                      child: Text(
                        data.age!,
                        style: customTextStyle(
                            size: 16,
                            weight: FontWeight.w700,
                            colors: MyColors.fontTitle),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 13, bottom: 17),
                      child: Text(
                        data.valueGender!,
                        style: customTextStyle(
                            size: 13,
                            weight: FontWeight.w500,
                            colors: MyColors.fontTitle),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 14, left: 13),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Wrap(spacing: 4, children: [
                      clipr(Icons.tiktok_rounded, data.hero),
                      clipr(Icons.pets, data.noneField),
                    ]),
                  ),
                ),
              ],
            );
  }

  Widget clipr(IconData? icon, String? label) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(20)),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
        child: Container(
          padding: const EdgeInsets.symmetric(vertical:9.5, horizontal: 16),
          decoration: BoxDecoration(
              gradient: RadialGradient(colors: [
            Colors.white.withOpacity(0.10),
            Colors.white.withOpacity(0.04)
          ])),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(icon, color: MyColors.fontTitle,size: 15,),
              const SizedBox(width: 5,),
              Text(
                label!,
                style: customTextStyle(
                    size: 14, weight: FontWeight.normal, colors: MyColors.fontTitle),
              ),
            ],
          ),
        ),
      ),
    );
  }

  BoxDecoration decoBoxImage() {
    return BoxDecoration(
        color: MyColors.card1,
        borderRadius: const BorderRadius.all(Radius.circular(16)),
        image: DecorationImage(image: FileImage(file!), fit: BoxFit.cover));
  }

  BoxDecoration decoBox() {
    return const BoxDecoration(
        color: MyColors.card1,
        borderRadius: BorderRadius.all(Radius.circular(16)));
  }
}
