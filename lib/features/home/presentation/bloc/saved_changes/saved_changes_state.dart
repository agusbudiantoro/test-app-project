part of 'saved_changes_cubit.dart';

abstract class SavedChangesState {
  ProfileModel? dataProfilState;
  TextEditingController? textDate;
  SavedChangesState({this.dataProfilState, this.textDate});
}

class SavedChangesInitial extends SavedChangesState {
  
  
}

class SavedChangeProfileSuccess extends SavedChangesState{
  ProfileModel? dataProfil;
  SavedChangeProfileSuccess({this.dataProfil});
}

class SavedChangeProfileFailed extends SavedChangesState{
  String? message;
  SavedChangeProfileFailed({this.message});
}

