import 'package:appyou/features/home/domain/models/model_profile.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

part 'saved_changes_state.dart';

class SavedChangesCubit extends Cubit<SavedChangesState> {
  SavedChangesCubit() : super(SavedChangesInitial());

  void simpanData(ProfileModel profileModel) async{
    try {
          profileModel.age = calculateAge(profileModel.ageFormatDate!);
      emit(SavedChangeProfileSuccess(dataProfil: profileModel));
    } catch (e) {
      emit(SavedChangeProfileFailed(message: e.toString()));
    }
  }

  String calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age.toString();
  }
}
