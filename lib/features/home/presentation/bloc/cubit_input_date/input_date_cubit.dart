import 'package:appyou/features/home/domain/models/model_profile.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

part 'input_date_state.dart';

class InputDateCubit extends Cubit<InputDateState> {
  InputDateCubit() : super(InputDateInitial());

  void inputDate(DateTime? date) async{
    try {
      TextEditingController text =  TextEditingController(text: DateFormat('MM/dd/yyyy').format(date!));
      emit(ChangeSelectDate(date: text, age: calculateAge(date)));
    } catch (e) {
      emit(ChangeSelectDateFailed(message: e.toString()));
    }
  }

  String calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age.toString();
  }
}
