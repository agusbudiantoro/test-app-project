part of 'input_date_cubit.dart';

abstract class InputDateState {
  ProfileModel? model;
  TextEditingController? textDate;
  InputDateState({this.model, this.textDate});
}

class InputDateInitial extends InputDateState {}
class ChangeSelectDate extends InputDateState{
  TextEditingController? date;
  String? age;
  ChangeSelectDate({this.date, this.age});
}
class ChangeSelectDateFailed extends InputDateState{
  String? message;
  ChangeSelectDateFailed({this.message});
}
