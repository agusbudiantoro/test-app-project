part of 'input_data_gender_cubit.dart';

abstract class InputDataGenderState {
  ProfileModel? model;
  InputDataGenderState({this.model});
}

class InputDataGenderInitial extends InputDataGenderState {}

class ChangeGenderDropdown extends InputDataGenderState{
  String? gender;
  ChangeGenderDropdown({this.gender});
}

class ChangeGenderDropdownFailed extends InputDataGenderState{
  String? message;
  ChangeGenderDropdownFailed({this.message});
}
