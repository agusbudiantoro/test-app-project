import 'package:appyou/features/home/domain/models/model_profile.dart';
import 'package:bloc/bloc.dart';

part 'input_data_gender_state.dart';

class InputDataGenderCubit extends Cubit<InputDataGenderState> {
  InputDataGenderCubit() : super(InputDataGenderInitial());

  void simpanDataGender(String? gender) async{
    try {
      emit(ChangeGenderDropdown(gender: gender));
    } catch (e) {
      emit(ChangeGenderDropdownFailed(message: e.toString()));
    }
  }
}
