part of 'upload_file_gambar_cubit.dart';

abstract class UploadFileGambarState {}

class UploadFileGambarInitial extends UploadFileGambarState {}
class UploadFileGambarSuccess extends UploadFileGambarState {
  File? file;
  UploadFileGambarSuccess({this.file});
}
class PindahMenuFotoSuccess extends UploadFileGambarState {
  bool? status;
  PindahMenuFotoSuccess({this.status});
}
class UploadFileGambarFailed extends UploadFileGambarState {
  String? message;
  UploadFileGambarFailed({this.message});
}
