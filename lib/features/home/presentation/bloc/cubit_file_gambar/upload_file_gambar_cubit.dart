import 'dart:io';

import 'package:bloc/bloc.dart';

part 'upload_file_gambar_state.dart';

class UploadFileGambarCubit extends Cubit<UploadFileGambarState> {
  UploadFileGambarCubit() : super(UploadFileGambarInitial());

  void uploadGambar(File? file) async{
    try {
      emit(UploadFileGambarSuccess(file: file));
    } catch (e) {
      emit(UploadFileGambarFailed(message: e.toString()));
    }
  }
  
  void pindahMenu(bool? status) async{
    try {
      emit(PindahMenuFotoSuccess(status: status));
    } catch (e) {
      emit(UploadFileGambarFailed(message: e.toString()));
    }
  }
}
