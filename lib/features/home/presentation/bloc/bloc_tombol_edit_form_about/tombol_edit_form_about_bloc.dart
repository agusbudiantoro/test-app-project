import 'package:bloc/bloc.dart';

part 'tombol_edit_form_about_event.dart';
part 'tombol_edit_form_about_state.dart';

class TombolEditFormAboutBloc extends Bloc<TombolEditFormAboutEvent, TombolEditFormAboutState> {
  TombolEditFormAboutBloc() : super(TombolEditFormAboutInitial()) {
    on<TombolEditFormAboutEvent>((event, emit) async{
        if(event is ChangeEditFormAboutEvent){
          emit(TombolEditFormAboutSuccess(status: event.status!));
        }
    });
  }
}
