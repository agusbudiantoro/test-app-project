part of 'tombol_edit_form_about_bloc.dart';

abstract class TombolEditFormAboutEvent {}
class ChangeEditFormAboutEvent extends TombolEditFormAboutEvent {
  bool? status;
  ChangeEditFormAboutEvent({this.status});
}


