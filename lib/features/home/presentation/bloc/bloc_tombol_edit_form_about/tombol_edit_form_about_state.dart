part of 'tombol_edit_form_about_bloc.dart';

abstract class TombolEditFormAboutState {}

class TombolEditFormAboutInitial extends TombolEditFormAboutState {}
class TombolEditFormAboutSuccess extends TombolEditFormAboutState {
  bool? status;
  TombolEditFormAboutSuccess({this.status});
}


