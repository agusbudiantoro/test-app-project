import 'dart:io';

import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/core/utils/widgets/appbar.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_file_gambar/upload_file_gambar_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_input_date/input_date_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/cubit_input_gender/input_data_gender_cubit.dart';
import 'package:appyou/features/home/presentation/bloc/saved_changes/saved_changes_cubit.dart';
import 'package:appyou/features/home/presentation/utils/form_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:photo_gallery/photo_gallery.dart';
import 'package:transparent_image/transparent_image.dart';

typedef MyFormValueGender = void Function(String);
typedef MyFormName = void Function(String);
typedef MyFormBirthday = void Function(String, DateTime);
typedef MyFormHero = void Function(String);
typedef MyFormNoneField = void Function(String);
typedef MyFormHeight = void Function(String);
typedef MyFormWeight = void Function(String);

class WidgetFormAbout extends StatefulWidget {
  const WidgetFormAbout({
    Key? key,
    required this.biggest,
    required this.cubitBlocUpload,
    required this.cubitBlocSavedData,
    required this.cubitBlocChangeDate,
    required this.cubitBlocChangeGender,
    this.valueGender,
    this.name,
    this.birthday,
    this.hero,
    this.noneField,
    this.heightBody,
    this.weightBody,
  }) : super(key: key);

  final Size biggest;
  final UploadFileGambarCubit cubitBlocUpload;
  final SavedChangesCubit cubitBlocSavedData;
  final InputDataGenderCubit cubitBlocChangeGender;
  final InputDateCubit cubitBlocChangeDate;
  final MyFormValueGender? valueGender;
  final MyFormName? name;
  final MyFormBirthday? birthday;
  final MyFormHero? hero;
  final MyFormNoneField? noneField;
  final MyFormHeight? heightBody;
  final MyFormWeight? weightBody;

  @override
  State<WidgetFormAbout> createState() => _WidgetFormAboutState();
}

class _WidgetFormAboutState extends State<WidgetFormAbout> {
  List<Album>? myalbums;
  bool loading = false;
  List<Medium> media = <Medium>[];
  String? valueDrop;
  List<String> items = ['Male', 'Female'];

  @override
  void initState() {
    super.initState();
  }

  Future<void> initAsync() async {
    if (await promptPermissionSetting()) {
      List<Album> albums =
          await PhotoGallery.listAlbums(mediumType: MediumType.image);
      setState(() {
        myalbums = albums;
        loading = false;
        modalGallery();
      });
    }
    setState(() {
      loading = false;
    });
  }

  Future<bool> promptPermissionSetting() async {
    if (Platform.isIOS &&
            await Permission.storage.request().isGranted &&
            await Permission.photos.request().isGranted ||
        Platform.isAndroid && await Permission.storage.request().isGranted) {
      return true;
    }
    return false;
  }

  modalGallery() {
    showModalBottomSheet(
        backgroundColor: MyColors.primaryBackground,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        context: context,
        builder: (context) {
          return BlocBuilder<UploadFileGambarCubit, UploadFileGambarState>(
            bloc: widget.cubitBlocUpload,
            builder: (context, state) {
              if (state is PindahMenuFotoSuccess) {
                if (state.status!) {
                  return widgetFoto();
                } else {
                  return widgetGallery();
                }
              }
              return widgetGallery();
            },
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
          left: 27, bottom: 17, top: 10, right: widget.biggest.width / 18),
      child: Column(
        children: [
          Container(
            color: Colors.transparent,
            width: widget.biggest.width,
            child: Row(
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      loading = true;
                    });
                    initAsync();
                  },
                  child:
                      BlocBuilder<UploadFileGambarCubit, UploadFileGambarState>(
                    bloc: widget.cubitBlocUpload,
                    builder: (context, state) {
                      if (state is UploadFileGambarSuccess) {
                        return Container(
                          width: widget.biggest.width / 7,
                          height: widget.biggest.width / 7,
                          decoration: BoxDecoration(
                              color: MyColors.card2,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(17)),
                              image: DecorationImage(
                                  image: FileImage(state.file!),
                                  fit: BoxFit.cover)),
                        );
                      }
                      return Container(
                        width: widget.biggest.width / 7,
                        height: widget.biggest.width / 7,
                        decoration: const BoxDecoration(
                          color: MyColors.card2,
                          borderRadius: BorderRadius.all(Radius.circular(17)),
                        ),
                        child: Icon(
                          Icons.add,
                          color: MyColors.fontTitle,
                          size: widget.biggest.width / 10,
                        ),
                      );
                    },
                  ),
                ),
                Container(
                  color: Colors.transparent,
                  padding: const EdgeInsets.only(left: 10),
                  child: Text(
                    "Add image",
                    style: customTextStyle(
                        size: 12,
                        weight: FontWeight.w500,
                        colors: MyColors.fontTitle),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: widget.biggest.height / 40,
          ),
          formHomeCustom(
              height: widget.biggest.height / 13,
              width: widget.biggest.width,
              myHint: "Enter name",
              label: "Display name:",
              clickCallback: (val) {
                widget.name!(val);
              }),
          BlocBuilder<InputDataGenderCubit, InputDataGenderState>(
            bloc: widget.cubitBlocChangeGender,
            builder: (context, state) {
              if (state is ChangeGenderDropdown) {
                return formHomeCustomSelectOption(
                    dropdownvalue: state.gender,
                    height: widget.biggest.height / 13,
                    width: widget.biggest.width,
                    myHint: "Select Gender",
                    label: "Gender:",
                    items: items,
                    clickCallback: (val) {
                      widget.valueGender!(val);
                      widget.cubitBlocChangeGender.simpanDataGender(val);
                    },
                    suffixIcon: Icons.keyboard_arrow_down_rounded);
              }
              return formHomeCustomSelectOption(
                  dropdownvalue: state.model?.valueGender,
                  height: widget.biggest.height / 13,
                  width: widget.biggest.width,
                  myHint: "Select Gender",
                  label: "Gender:",
                  items: items,
                  clickCallback: (val) {
                    widget.valueGender!(val);
                    widget.cubitBlocChangeGender.simpanDataGender(val);
                  },
                  suffixIcon: Icons.keyboard_arrow_down_rounded);
            },
          ),
          BlocBuilder<InputDateCubit, InputDateState>(
            bloc: widget.cubitBlocChangeDate,
            builder: (context, state) {
              if (state is ChangeSelectDate) {
                return formHomeCustomDate(
                    context: context,
                    controller: state.date,
                    height: widget.biggest.height / 13,
                    width: widget.biggest.width,
                    myHint: "DD MM YYYY",
                    label: "Birthday:",
                    clickCallback: (val) {
                      String? text = DateFormat('MM/dd/yyyy').format(val);
                      widget.birthday!(text,val);
                    widget.cubitBlocChangeDate.inputDate(val);
                    });
              }
              return formHomeCustomDate(
                  context: context,
                  controller: state.textDate,
                  height: widget.biggest.height / 13,
                  width: widget.biggest.width,
                  myHint: "DD MM YYYY",
                  label: "Birthday:",
                  clickCallback: (val) {
                    String? text = DateFormat('MM/dd/yyyy').format(val);
                      widget.birthday!(text,val);
                    widget.cubitBlocChangeDate.inputDate(val);
                  });
            },
          ),
          formHomeCustom(
              height: widget.biggest.height / 13,
              width: widget.biggest.width,
              myHint: "--",
              label: "Horoscope:",
              clickCallback: (val) {
                widget.hero!(val);
              }),
          formHomeCustom(
              height: widget.biggest.height / 13,
              width: widget.biggest.width,
              myHint: "--",
              label: "none:",
              clickCallback: (val) {
                widget.noneField!(val);
              }),
          formHomeCustom(
              height: widget.biggest.height / 13,
              width: widget.biggest.width,
              myHint: "Add height",
              label: "Height:",
              clickCallback: (val) {
                widget.heightBody!(val);
              }),
          formHomeCustom(
              height: widget.biggest.height / 13,
              width: widget.biggest.width,
              myHint: "Add weight",
              label: "Weight:",
              clickCallback: (val) {
                widget.weightBody!(val);
              })
        ],
      ),
    );
  }

  Widget widgetGallery() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      color: Colors.transparent,
      width: widget.biggest.width,
      child: (loading)
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : LayoutBuilder(builder: (context, constraints) {
              double gridWidth = (constraints.maxWidth - 20) / 3;
              double gridHeight = gridWidth + 33;
              double ratio = gridWidth / gridHeight;
              return Container(
                padding: const EdgeInsets.all(5),
                child: GridView.count(
                  childAspectRatio: ratio,
                  crossAxisCount: 3,
                  mainAxisSpacing: 5.0,
                  crossAxisSpacing: 5.0,
                  children: <Widget>[
                    ...?myalbums?.map(
                      (album) => GestureDetector(
                        onTap: () {
                          initAsyncFoto(album);
                        },
                        child: Column(
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(5.0),
                              child: Container(
                                color: Colors.grey[300],
                                height: gridWidth,
                                width: gridWidth,
                                child: FadeInImage(
                                  fit: BoxFit.cover,
                                  placeholder: MemoryImage(kTransparentImage),
                                  image: AlbumThumbnailProvider(
                                    albumId: album.id,
                                    mediumType: album.mediumType,
                                    highQuality: true,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              padding: const EdgeInsets.only(left: 2.0),
                              child: Text(
                                album.name ?? "Unnamed Album",
                                maxLines: 1,
                                textAlign: TextAlign.start,
                                style: customTextStyle(
                                    size: 14,
                                    height: 1.2,
                                    weight: FontWeight.normal,
                                    colors: MyColors.fontTitle),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              padding: const EdgeInsets.only(left: 2.0),
                              child: Text(album.count.toString(),
                                  textAlign: TextAlign.start,
                                  style: customTextStyle(
                                      size: 12,
                                      height: 1.2,
                                      weight: FontWeight.normal,
                                      colors: MyColors.fontTitle)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }),
    );
  }

  void initAsyncFoto(Album album) async {
    MediaPage mediaPage = await album.listMedia();
    setState(() {
      media = mediaPage.items;
    });
    widget.cubitBlocUpload.pindahMenu(true);
  }

  Widget widgetFoto() {
    return Container(
      color: MyColors.primaryBackground3,
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: AppbarCustom(clickCallBack: (val) {
              widget.cubitBlocUpload.pindahMenu(false);
            }),
          ),
          Expanded(
            flex: 12,
            child: GridView.count(
              crossAxisCount: 3,
              mainAxisSpacing: 1.0,
              crossAxisSpacing: 1.0,
              children: <Widget>[
                ...media.map(
                  (medium) => GestureDetector(
                    onTap: () {
                      medium.getFile().then((value) {
                        widget.cubitBlocUpload.uploadGambar(value);
                        Navigator.pop(context);
                      });
                    },
                    child: Container(
                      color: Colors.grey[300],
                      child: FadeInImage(
                        fit: BoxFit.cover,
                        placeholder: MemoryImage(kTransparentImage),
                        image: ThumbnailProvider(
                          mediumId: medium.id,
                          mediumType: medium.mediumType,
                          highQuality: true,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
