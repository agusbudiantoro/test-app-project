import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:flutter/material.dart';

typedef MyFormField = void Function(String);
typedef MyFormFieldDate = void Function(DateTime);
Widget formHomeCustom(
    {double? height,
    double? width,
    String? myHint,
    IconData? suffixIcon,
    String? label,
    MyFormField? clickCallback}) {
  return Container(
    color: Colors.transparent,
    height: height,
    width: width,
    child: Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              flex: 1,
              child: Text(
                label!,
                style: customTextStyle(
                    size: 13,
                    weight: FontWeight.w500,
                    colors: MyColors.hintText),
              )),
          Expanded(
            flex: 2,
            child: formCustom(
                height: height,
                width: width,
                myHint: myHint,
                suffixIcon: suffixIcon,
                clickCallback: (val) {
                  clickCallback!(val);
                }),
          )
        ],
      ),
    ),
  );
}
Widget formHomeCustomDate(
    {double? height,
    double? width,
    String? myHint,
    IconData? suffixIcon,
    String? label,
    MyFormFieldDate? clickCallback,
    BuildContext? context,
    TextEditingController? controller}) {
  return Container(
    color: Colors.transparent,
    height: height,
    width: width,
    child: Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              flex: 1,
              child: Text(
                label!,
                style: customTextStyle(
                    size: 13,
                    weight: FontWeight.w500,
                    colors: MyColors.hintText),
              )),
          Expanded(
            flex: 2,
            child: formCustomDate(
                context: context,
                height: height,
                width: width,
                myHint: myHint,
                controller: controller,
                suffixIcon: suffixIcon,
                clickCallback: (val) {
                  clickCallback!(val);
                }),
          )
        ],
      ),
    ),
  );
}

Widget formHomeCustomSelectOption(
    {double? height,
    double? width,
    String? myHint,
    IconData? suffixIcon,
    String? label,
    MyFormField? clickCallback,
    String? dropdownvalue,
    List<String>? items }) {
  return Container(
    color: Colors.transparent,
    height: height,
    width: width,
    child: Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              flex: 1,
              child: Text(
                label!,
                style: customTextStyle(
                    size: 13,
                    weight: FontWeight.w500,
                    colors: MyColors.hintText),
              )),
          Expanded(
            flex: 2,
            child: customTextFieldSelectOption(
                height: height,
                width: width,
                myHint: myHint,
                dropdownvalue: dropdownvalue,
                items: items,
                clickCallback: (val) {
                  clickCallback!(val);
                }),
          )
        ],
      ),
    ),
  );
}

Widget formCustom(
    {double? height,
    double? width,
    String? myHint,
    IconData? suffixIcon,
    MyFormField? clickCallback}) {
  return ClipRRect(
    borderRadius: const BorderRadius.all(Radius.circular(10)),
    child: Container(
      height: height,
      width: width,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 18),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: MyColors.fontTitle.withOpacity(0.22)),
          gradient: RadialGradient(radius: 20, colors: [
            Colors.white.withOpacity(0.10),
            Colors.white.withOpacity(0.3)
          ])),
      child: Row(
        children: [
          Expanded(
              child: TextField(
            textAlign: TextAlign.end,
            textAlignVertical: TextAlignVertical.center,
            onChanged: (val) {
              clickCallback!(val);
            },
            minLines: 1,
            maxLines: 1,
            style: customTextStyle(
                size: 13, weight: FontWeight.w500, colors: MyColors.fontTitle),
            decoration: InputDecoration(
                hintText: myHint,
                hintStyle: customTextStyle(
                    size: 13,
                    weight: FontWeight.w500,
                    colors: MyColors.hintText),
                border: InputBorder.none),
          )),
          GestureDetector(
              onTap: () {},
              child: suffixIcon != null
                  ? Icon(
                      suffixIcon,
                      color: Colors.white,
                    )
                  : Container())
        ],
      ),
    ),
  );
}

Widget formCustomDate(
    {double? height,
    double? width,
    String? myHint,
    IconData? suffixIcon,
    MyFormFieldDate? clickCallback,
    BuildContext? context,
    TextEditingController? controller
    }) {
  return ClipRRect(
    borderRadius: const BorderRadius.all(Radius.circular(10)),
    child: Container(
      height: height,
      width: width,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 18),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: MyColors.fontTitle.withOpacity(0.22)),
          gradient: RadialGradient(radius: 20, colors: [
            Colors.white.withOpacity(0.10),
            Colors.white.withOpacity(0.3)
          ])),
      child: Row(
        children: [
          Expanded(
              child: TextField(
                controller: controller,
            textAlign: TextAlign.end,
            textAlignVertical: TextAlignVertical.center,
            onTap: ()async{
              var date = await showDatePicker(
                      context: context!,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(1900),
                      lastDate: DateTime(2100));
                  if (date != null) {
                    clickCallback!(date);
                  }

            },
            minLines: 1,
            maxLines: 1,
            style: customTextStyle(
                size: 13, weight: FontWeight.w500, colors: MyColors.fontTitle),
            decoration: InputDecoration(
                hintText: myHint,
                hintStyle: customTextStyle(
                    size: 13,
                    weight: FontWeight.w500,
                    colors: MyColors.hintText),
                border: InputBorder.none),
          )),
          GestureDetector(
              onTap: () {},
              child: suffixIcon != null
                  ? Icon(
                      suffixIcon,
                      color: Colors.white,
                    )
                  : Container())
        ],
      ),
    ),
  );
}

Widget customTextFieldSelectOption(
    {double? height,
    double? width,
    String? myHint,
    MyFormField? clickCallback,
    String? dropdownvalue,
    List<String>? items }) {
       return ClipRRect(
    borderRadius: const BorderRadius.all(Radius.circular(10)),
    child: Container(
      height: height,
      width: width,
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 18),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: MyColors.fontTitle.withOpacity(0.22)),
          gradient: RadialGradient(radius: 20, colors: [
            Colors.white.withOpacity(0.10),
            Colors.white.withOpacity(0.3)
          ])),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          DropdownButton(
            alignment: Alignment.centerRight,
          value: dropdownvalue,
          dropdownColor: MyColors.primaryBackground3,
          hint: Text(myHint!,textAlign: TextAlign.end, style: customTextStyle(
            size: 13, weight: FontWeight.w500, colors: MyColors.hintText)),
          icon: const Icon(Icons.keyboard_arrow_down),    
          items: items?.map((String items) {
            return DropdownMenuItem(
          value: items,
          child: Text(items,textAlign: TextAlign.end, style: customTextStyle(
            size: 13, weight: FontWeight.w500, colors: MyColors.fontTitle)),
            );
          }).toList(),
          underline: Container(),
          style: customTextStyle(
            size: 13, weight: FontWeight.w500, colors: MyColors.fontTitle),
          onChanged: (String? newValue) { 
            clickCallback!(newValue!);
          },
        ),
        ],
      ),
    ),
  );
}
