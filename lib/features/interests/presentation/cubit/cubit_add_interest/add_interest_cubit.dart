import 'package:appyou/features/interests/domain/models/interest_model.dart';
import 'package:bloc/bloc.dart';

part 'add_interest_state.dart';

class AddInterestCubit extends Cubit<AddInterestState> {
  AddInterestCubit() : super(AddInterestInitial());
  
  void addInterest(ModelInterest data,List<ModelInterest> dataLama) {
    emit(AddInterestWaiting());
    try {
      dataLama.add(ModelInterest(id: data.id, title: data.title));
      emit(AddInterestSuccess(interest: dataLama));
    } catch (e) {
      emit(AddInterestFailed(message: e.toString()));
    }
  
  }

void deleteInterest(ModelInterest data,List<ModelInterest> dataLama) {
    emit(AddInterestWaiting());
    try {
      dataLama.removeWhere((item)=> item.id == data.id);
      emit(AddInterestSuccess(interest: dataLama));
    } catch (e) {
      emit(AddInterestFailed(message: e.toString()));
    }
  }
  
  void saveInterest(List<ModelInterest> data) {
    emit(AddSaveInterestWaiting());
    try {
      emit(AddSaveInterestSuccess(interest: data));
    } catch (e) {
      emit(AddSaveInterestFailed(message: e.toString()));
    }
  }
}
