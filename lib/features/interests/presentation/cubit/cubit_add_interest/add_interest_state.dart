part of 'add_interest_cubit.dart';

abstract class AddInterestState {}

class AddInterestInitial extends AddInterestState {}
class AddInterestSuccess extends AddInterestState {
  List<ModelInterest>? interest;
  AddInterestSuccess({this.interest});
}
class AddInterestWaiting extends AddInterestState {}
class AddInterestFailed extends AddInterestState {
  String? message;
  AddInterestFailed({this.message});
}

class AddSaveInterestSuccess extends AddInterestState {
  List<ModelInterest>? interest;
  AddSaveInterestSuccess({this.interest});
}
class AddSaveInterestWaiting extends AddInterestState {}
class AddSaveInterestFailed extends AddInterestState {
  String? message;
  AddSaveInterestFailed({this.message});
}
