import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/interests/data/data_generator.dart';
import 'package:appyou/features/interests/domain/models/interest_model.dart';
import 'package:appyou/features/interests/presentation/cubit/cubit_add_interest/add_interest_cubit.dart';
import 'package:appyou/features/interests/presentation/widgets/appbar.dart';
import 'package:appyou/features/interests/presentation/widgets/form_multiselect.dart';
import 'package:appyou/features/interests/presentation/widgets/title.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InterestScreen extends StatelessWidget {
  const InterestScreen({Key? key, required this.cubitAddInterest, required this.cubitAddSaveInterest}) : super(key: key);
  final AddInterestCubit cubitAddInterest;
  final AddInterestCubit cubitAddSaveInterest;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
        begin: Alignment.topRight,
        end: Alignment.bottomLeft,
        colors: [
          MyColors.primaryBackground,
          MyColors.primaryBackground2,
          MyColors.primaryBackground3,
        ],
      )),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SafeArea(
          child: Container(
            color: Colors.transparent,
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              physics: const AlwaysScrollableScrollPhysics(),
              child: Column(
                children: [
                  AppbarCustomInterest(clickCallBack: (val) {
                    Navigator.pop(context);
                  },
                  cubitAddInterest: cubitAddInterest,
                  cubitAddSaveInterest: cubitAddSaveInterest,
                  ),
                  titleInterest(size),
                  BlocBuilder<AddInterestCubit, AddInterestState>(
                    bloc: cubitAddInterest,
                    builder: (context, state) {
                      List<ModelInterest> dataInt = getListInterest();
                      if(state is AddInterestSuccess){
                        return formMultiSelect(
                          context: context,
                          size: size,
                          data: dataInt,
                          cubitAddInterest:cubitAddInterest,
                          buttonTap: (val) {
                            cubitAddInterest.addInterest(val, state.interest!);
                            Navigator.pop(context);
                          });
                      }
                      return formMultiSelect(
                          context: context,
                          size: size,
                          data: dataInt,
                          cubitAddInterest:cubitAddInterest,
                          buttonTap: (val) {
                            cubitAddInterest.addInterest(val,<ModelInterest>[]);
                            Navigator.pop(context);
                          });
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
