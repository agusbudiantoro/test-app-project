import 'dart:ui';

import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/interests/domain/models/interest_model.dart';
import 'package:appyou/features/interests/presentation/cubit/cubit_add_interest/add_interest_cubit.dart';
import 'package:appyou/features/interests/presentation/widgets/chips.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef MyButtonTap = void Function(ModelInterest);
Widget formMultiSelect(
    {BuildContext? context,
    Size? size,
    List<ModelInterest>? data,
    MyButtonTap? buttonTap,
    AddInterestCubit? cubitAddInterest}) {
  return Padding(
    padding: EdgeInsets.symmetric(
        vertical: size!.height / 24, horizontal: size.width / 15),
    child: GestureDetector(
      onTap: () {
        modalListInterest(
            context: context,
            size: size,
            data: data,
            buttonTap: (val) {
              buttonTap!(val);
            });
      },
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: Container(
            width: size.width,
            decoration: BoxDecoration(
                gradient: RadialGradient(radius: 20, colors: [
              Colors.white.withOpacity(0.06),
              Colors.white.withOpacity(0.10)
            ])),
            child: BlocBuilder<AddInterestCubit, AddInterestState>(
              bloc: cubitAddInterest,
              builder: (context, state) {
                if (state is AddInterestSuccess) {
                  if(state.interest!.isEmpty){
                    return Container(
                      color: Colors.transparent,
                      height: size.height / 15,
                    );
                  }else{
                    return chipsDynamis(state.interest!, cubitAddInterest!);
                  }
                }
                return Container(
                  color: Colors.transparent,
                  height: size.height / 15,
                );
              },
            ),
          ),
        ),
      ),
    ),
  );
}

modalListInterest(
    {BuildContext? context,
    Size? size,
    List<ModelInterest>? data,
    MyButtonTap? buttonTap}) {
  showModalBottomSheet(
      backgroundColor: MyColors.primaryBackground2,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      context: context!,
      builder: (context) {
        return Container(
          color: Colors.transparent,
          height: size!.height / 3,
          width: size.width,
          child: ListView.builder(
              itemCount: data?.length,
              shrinkWrap: true,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    buttonTap!(data[index]);
                  },
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 15),
                        color: Colors.transparent,
                        width: size.width,
                        child: Text(
                          data![index].title!,
                          style: customTextStyle(
                              colors: MyColors.fontTitle,
                              size: 14,
                              weight: FontWeight.w500),
                        ),
                      ),
                      const Divider(
                        height: 10,
                        color: MyColors.fontTitle,
                      )
                    ],
                  ),
                );
              }),
        );
      });
}
