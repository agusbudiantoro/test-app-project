import 'package:appyou/core/styles/fonts.dart';
import 'package:flutter/material.dart';

Widget titleInterest(Size size){
  return Container(
    padding: EdgeInsets
    .only(left: size.width/11, top: size.height/11),
    alignment: Alignment.centerLeft,
    color: Colors.transparent,
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Tell everyone about yourself", style: customTextStyleGradient(weight: FontWeight.w700,size: 14),),
          ],
        ),
        SizedBox(height: size.height/70,),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("What interest you?", style: customTextStyle(weight: FontWeight.w700,size: 20),),
          ],
        ),
      ],
    ),
  );
}