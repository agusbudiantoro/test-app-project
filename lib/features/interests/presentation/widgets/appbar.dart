import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/interests/presentation/cubit/cubit_add_interest/add_interest_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef ButtonCallBack = void Function(String);

class AppbarCustomInterest extends StatelessWidget {
  const AppbarCustomInterest(
      {Key? key, this.clickCallBack, required this.cubitAddSaveInterest, required this.cubitAddInterest})
      : super(key: key);
  final ButtonCallBack? clickCallBack;
  final AddInterestCubit cubitAddSaveInterest;
  final AddInterestCubit cubitAddInterest;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      color: Colors.transparent,
      width: size.width,
      height: size.height / 12,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.transparent,
              child: GestureDetector(
                onTap: () {
                  clickCallBack!("berhasil");
                },
                child: Row(
                  children: const [
                    Expanded(
                      flex: 1,
                      child: Icon(Icons.arrow_back_ios_new,
                          size: 14, color: MyColors.fontTitle),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      flex: 14,
                      child: Text(
                        "Back",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            wordSpacing: 0.5,
                            color: MyColors.fontTitle,
                            fontFamily: 'Inter'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
                padding: const EdgeInsets.only(right: 26),
                alignment: Alignment.centerRight,
                color: Colors.transparent,
                child: BlocBuilder<AddInterestCubit, AddInterestState>(
                  bloc: cubitAddInterest,
                  builder: (context, state) {
                    if(state is AddInterestSuccess){
                      return GestureDetector(
                        onTap: () {
                          cubitAddSaveInterest.saveInterest(state.interest!);
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Save",
                          style: customTextStyleGradientInterestScreen(
                              weight: FontWeight.w500, size: 14),
                        ));
                    }
                    return GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Save",
                          style: customTextStyleGradientInterestScreen(
                              weight: FontWeight.w500, size: 14),
                        ));
                  },
                )),
          ),
        ],
      ),
    );
  }
}
