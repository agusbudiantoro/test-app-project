import 'dart:ui';

import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:appyou/features/interests/domain/models/interest_model.dart';
import 'package:appyou/features/interests/presentation/cubit/cubit_add_interest/add_interest_cubit.dart';
import 'package:flutter/material.dart';

Widget chipsDynamis(List<ModelInterest> data,AddInterestCubit cubitInterest){
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: const EdgeInsets.only(left: 17, right: 17, top: 8),
        child: Wrap(
          spacing: 4, children: data.map((e) => clipr(e,cubitInterest,data)).toList()),
      ),
    );
  }

  Widget clipr(ModelInterest? data,AddInterestCubit cubitInterest, List<ModelInterest> oldData) {
    return Padding(
      padding: const EdgeInsets.only(left: 2, top: 0, right: 2, bottom: 8),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(4)),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            decoration: BoxDecoration(
                gradient: RadialGradient(colors: [
              Colors.white.withOpacity(0.10),
              Colors.white.withOpacity(0.04)
            ])),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  data!.title!,
                  style: customTextStyle(
                      size: 14, weight: FontWeight.normal, colors: MyColors.fontTitle),
                ),
                const SizedBox(width: 5,),
                GestureDetector(
                  onTap: (){
                    cubitInterest.deleteInterest(data, oldData);
                  },
                  child: const Icon(Icons.close, color: MyColors.fontTitle,size: 14,))
              ],
            ),
          ),
        ),
      ),
    );
  }