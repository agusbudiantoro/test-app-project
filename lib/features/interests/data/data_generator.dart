import 'package:appyou/features/interests/domain/models/interest_model.dart';

List<ModelInterest> getListInterest(){
  List<ModelInterest> list = <ModelInterest>[];
  list.add(ModelInterest(id: 1, title: "Music"));
  list.add(ModelInterest(id: 2, title: "Basketball"));
  list.add(ModelInterest(id: 3, title: "Fitness"));
  list.add(ModelInterest(id: 4, title: "Gymming"));
  list.add(ModelInterest(id: 5, title: "Programming"));
  list.add(ModelInterest(id: 6, title: "Football"));
  list.add(ModelInterest(id: 7, title: "Futsal"));
  list.add(ModelInterest(id: 8, title: "Badminton"));
  return list;
}