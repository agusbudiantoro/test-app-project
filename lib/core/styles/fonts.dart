import 'package:appyou/core/utils/colors.dart';
import 'package:flutter/cupertino.dart';

TextStyle customTextStyle({double? size,FontWeight? weight, Color colors=MyColors.fontTitle, double? height }) {

  return TextStyle(
      fontSize: size,
      fontWeight: weight,
      wordSpacing: 0.5,
      color: colors,
      height: height,
      fontFamily: 'Inter');
}

TextStyle customTextStyleGradient({double? size,FontWeight? weight, Color colors=MyColors.fontTitle }) {
final Shader linearGradient = const LinearGradient(
  colors: <Color>[Color(0xff94783E), Color(0xffF3EDA6),Color(0xffF8FAE5),Color(0xffFFE2BE),Color(0xffD5BE88),Color(0xffF8FAE5),Color(0xffD5BE88)],
).createShader(const Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));
  return TextStyle(
      fontSize: size,
      fontWeight: weight,
      wordSpacing: 0.5,
      foreground: Paint()..shader = linearGradient,
      fontFamily: 'Inter');
}

TextStyle customTextStyleGradientInterestScreen({double? size,FontWeight? weight, Color colors=MyColors.fontTitle }) {
final Shader linearGradient = const LinearGradient(
  colors: <Color>[Color(0xffABFFFD), Color(0xff4599DB),Color(0xffAADAFF)],
).createShader(const Rect.fromLTWH(200.0, 20.0, 500.0, 10.0));
  return TextStyle(
      fontSize: size,
      fontWeight: weight,
      wordSpacing: 0.5,
      foreground: Paint()..shader = linearGradient,
      fontFamily: 'Inter');
}
