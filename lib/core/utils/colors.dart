import 'package:flutter/material.dart';

class MyColors {
  static const primaryBackground = Color(0xFF1F4247);
  static const primaryBackground2 = Color(0xFF0D1D23);
  static const primaryBackground3 = Color(0xFF09141A);
  static const card1 = Color(0xFF162329);
  static const card2 = Color(0xff212b31);
  static const secondaryBackground = Color.fromARGB(255, 29, 63, 68);
  static const thirdBackground = Color.fromARGB(255, 5, 40, 37);
  static const fontTitle = Color(0xFFe0e2e4);
  static const fontSubtitle = Color.fromARGB(255, 0, 0, 0);
  static const fontText = Color.fromARGB(255, 205, 202, 200);
  static const fontText2 = Color(0xFFA8C7CB);
  static const button1 = Color(0xFF62CDCB);
  static const button2 = Color(0xFF4599DB);
  static const hintText = Color.fromARGB(40, 255, 255, 255);
  
}