import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
typedef MyFormField = void Function();
Widget customButton({double? height, double? width, String? textTitle,bool status=false, MyFormField? clickCallback}) {
  return GestureDetector(
    onTap: (){
      clickCallback!();
    },
    child: Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        boxShadow:[
          (status)? const BoxShadow(
            color: MyColors.button2,
            blurRadius: 30,
            offset: Offset(0, 5)
          ):const BoxShadow(),
        ],
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            (status)?MyColors.button2:MyColors.button2.withOpacity(0.3),
            (status)?MyColors.button1:MyColors.button1.withOpacity(0.3),
          ],
        ),
      ),
      child: Center(
        child: Text(textTitle!, style: customTextStyle(
          size: 16,weight: FontWeight.w700,colors:(status)?MyColors.fontTitle: MyColors.fontTitle.withOpacity(0.4)),),
      ),
    ),
  );
}
