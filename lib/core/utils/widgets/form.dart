import 'dart:ui';

import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:flutter/material.dart';
typedef MyFormField = void Function(String);
typedef MyButtoIcon = void Function(bool);
Widget customTextField(
    {double? height,
    double? width,
    String? myHint,
    bool? obs,
    IconData? suffixIcon,
    MyFormField? clickCallback,
    MyButtoIcon? buttonIcon,
    }) {
  return ClipRRect(
    borderRadius: const BorderRadius.all(Radius.circular(10)),
    child: BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
      child: Container(
        height: height,
        width: width,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 18),
        decoration: BoxDecoration(
            gradient: RadialGradient(radius: 20, colors: [
          Colors.white.withOpacity(0.10),
          Colors.white.withOpacity(0.1)
        ])),
        child: Row(
          children: [
            Expanded(
              child: TextField(
                    onChanged: (val){
                      clickCallback!(val);
                    },
                    obscureText: (obs != null)?obs:false,
                    minLines: 1,
                    maxLines: 1,
                    style: customTextStyle(
                        size: 13,
                        weight: FontWeight.w500,
                        colors: MyColors.fontTitle),
                    decoration: InputDecoration(
                        hintText: myHint,
                        hintStyle: customTextStyle(
                        size: 13,
                        weight: FontWeight.w500,
                        colors: MyColors.hintText),
                        border: InputBorder.none),
                  )
            ),
            GestureDetector(
                onTap: () {
                  if(obs != null){
                    buttonIcon!(obs);
                  }
                  
                },
                child: suffixIcon != null
                    ? Icon(
                        suffixIcon,
                        color: Colors.white,
                      )
                    : Container())
          ],
        ),
      ),
    ),
  );
}
