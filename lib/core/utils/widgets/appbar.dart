import 'package:appyou/core/utils/colors.dart';
import 'package:flutter/material.dart';
typedef ButtonCallBack = void Function(String);
class AppbarCustom extends StatelessWidget {
  const AppbarCustom({Key? key, this.clickCallBack }) : super(key: key);
  final ButtonCallBack? clickCallBack;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      color: Colors.transparent,
      width: size.width,
      height: size.height / 12,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.transparent,
              child: GestureDetector(
                onTap: (){
                  clickCallBack!("berhasil");
                },
                child: Row(
                  children: const [
                    Expanded(
                      flex: 1,
                      child: Icon(Icons.arrow_back_ios_new, size: 14,color: MyColors.fontTitle),
                    ),
                    Expanded(
                      flex: 14,
                      child: Text(
                        "Back",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            wordSpacing: 0.5,
                            color: MyColors.fontTitle,
                            fontFamily: 'Inter'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
