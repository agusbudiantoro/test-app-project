import 'package:bloc/bloc.dart';

part 'require_form_event.dart';
part 'require_form_state.dart';

class RequireFormBloc extends Bloc<RequireFormEvent, RequireFormState> {
  RequireFormBloc() : super(RequireFormSuccess(status: false)) {
    on<RequireFormEvent>((event, emit) async{
      if(event is RequireFormEventLogin){
        if(event.username!.isNotEmpty && event.password!.isNotEmpty){
          emit(RequireFormSuccess(password: event.password, username: event.username, status: true));
        }else{
          emit(RequireFormFailed());
        }
      }


      if(event is RequireFormEventRegister){
        if(event.username!.isNotEmpty && event.password!.isNotEmpty && event.email!.isNotEmpty && event.passwordConfirm!.isNotEmpty){
          if(event.password == event.passwordConfirm){
            emit(RequireFormSuccess(password: event.password, username: event.username, email: event.email, passwordConfirm: event.passwordConfirm, status: true));
          }else{
          emit(RequireFormInitial());
          }
        }else{
        emit(RequireFormInitial());
        }
      }
    });
  }
}
