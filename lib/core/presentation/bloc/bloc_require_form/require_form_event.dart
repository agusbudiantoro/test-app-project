part of 'require_form_bloc.dart';

abstract class RequireFormEvent {}
class RequireFormEventLogin extends RequireFormEvent {
  final String? username;
  final String? password;
  RequireFormEventLogin({this.password,this.username});
}

class RequireFormEventRegister extends RequireFormEvent {
  final String? username;
  final String? password;
  final String? email;
  final String? passwordConfirm;
  RequireFormEventRegister({this.password,this.username, this.email, this.passwordConfirm});
}
