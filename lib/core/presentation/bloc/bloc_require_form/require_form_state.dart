part of 'require_form_bloc.dart';

abstract class RequireFormState {}

class RequireFormInitial extends RequireFormState {}
class RequireFormFailed extends RequireFormState {}
class RequireFormSuccess extends RequireFormState {
  final String? username;
  final String? password;
  final String? email;
  final String? passwordConfirm;
  final bool? status;
  RequireFormSuccess({this.status,this.email, this.password, this.passwordConfirm, this.username});
}
