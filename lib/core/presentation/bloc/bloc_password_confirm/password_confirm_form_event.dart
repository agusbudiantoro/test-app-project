part of 'password_confirm_form_bloc.dart';

abstract class PasswordConfirmFormEvent {}

class EventFieldPasswordConfirmEvent extends PasswordConfirmFormEvent{
  String? password;
  EventFieldPasswordConfirmEvent({this.password});
}
