import 'package:bloc/bloc.dart';

part 'password_confirm_form_event.dart';
part 'password_confirm_form_state.dart';

class PasswordConfirmFormBloc extends Bloc<PasswordConfirmFormEvent, PasswordConfirmFormState> {
  PasswordConfirmFormBloc() : super(StateSuccesTextFieldPasswordConfirm(password: "")) {
    on<PasswordConfirmFormEvent>((event, emit)async {
      if(event is EventFieldPasswordConfirmEvent){
        emit(StateSuccesTextFieldPasswordConfirm(password: event.password));
      }
    });
  }
}
