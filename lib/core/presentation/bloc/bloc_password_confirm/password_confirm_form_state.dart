part of 'password_confirm_form_bloc.dart';

abstract class PasswordConfirmFormState {}

class PasswordConfirmFormInitial extends PasswordConfirmFormState {}

class StateSuccesTextFieldPasswordConfirm extends PasswordConfirmFormState {
  String? password;
  StateSuccesTextFieldPasswordConfirm({this.password});
}
