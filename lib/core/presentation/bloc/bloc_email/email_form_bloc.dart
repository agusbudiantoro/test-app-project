import 'package:bloc/bloc.dart';

part 'email_form_event.dart';
part 'email_form_state.dart';

class EmailBloc extends Bloc<EmailEvent, EmailState> {
  EmailBloc() : super(StateSuccesTextFieldEmail(email: "")) {
    on<EmailEvent>((event, emit) async{
      
      if(event is EventFieldEmailEvent){
        emit(StateSuccesTextFieldEmail(email: event.email));
      }
    });
  }
}
