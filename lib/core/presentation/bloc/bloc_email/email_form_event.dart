part of 'email_form_bloc.dart';


abstract class EmailEvent {}
class EventEmailEvent extends EmailEvent{
}

class EventFieldEvent extends EmailEvent{
  String? thisfield;
  String? label;
  EventFieldEvent({this.thisfield, this.label});
}

class EventFieldEmailEvent extends EmailEvent{
  String? email;
  EventFieldEmailEvent({this.email});
}
