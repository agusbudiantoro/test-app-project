part of 'email_form_bloc.dart';

abstract class EmailState {}

class EmailInitial extends EmailState {
}

class StateSuccesTextFieldEmail extends EmailState {
  String? email;
  StateSuccesTextFieldEmail({this.email});
}
