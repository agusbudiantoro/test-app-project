import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'username_form_event.dart';
part 'username_form_state.dart';

class UsernameBloc extends Bloc<UsernameEvent, UsernameState> {
  UsernameBloc() : super(StateSuccesTextFieldUsername(username: "")) {
    on<UsernameEvent>((event, emit) async {
      if (event is EventFieldUsernameEvent) {
        emit(StateWaitingTextFieldUsername());
        try {
          emit(StateSuccesTextFieldUsername(username: event.username));
        } catch (e) {
          emit(StateFailedTextFieldUsername(message: e.toString()));
        }
      }
    });
  }
}
