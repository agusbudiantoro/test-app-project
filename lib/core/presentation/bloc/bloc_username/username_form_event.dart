part of 'username_form_bloc.dart';


abstract class UsernameEvent {}
class EventUsernameEvent extends UsernameEvent{
  TextEditingController? email;
  TextEditingController? password;
  EventUsernameEvent({this.email, this.password});
}

class EventFieldEvent extends UsernameEvent{
  String? thisfield;
  String? label;
  EventFieldEvent({this.thisfield, this.label});
}

class EventFieldUsernameEvent extends UsernameEvent{
  String? username;
  EventFieldUsernameEvent({this.username});
}
