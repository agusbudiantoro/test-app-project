part of 'username_form_bloc.dart';

@immutable
abstract class UsernameState extends Equatable {
  @override
  List<Object> get props => [];
}

class UsernameInitial extends UsernameState {
  final bool status;
  final TextEditingController? username;
  final TextEditingController? password;
  UsernameInitial({this.status = false, this.password, this.username});
}

class StateSuccesTextFieldUsername extends UsernameState {
  final String? username;
  StateSuccesTextFieldUsername({this.username});
  @override
  List<Object> get props => [username!];
}

class StateWaitingTextFieldUsername extends UsernameState {
  StateWaitingTextFieldUsername();
}

class StateFailedTextFieldUsername extends UsernameState {
  final String? message;
  StateFailedTextFieldUsername({this.message});
}
