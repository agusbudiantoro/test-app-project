import 'package:bloc/bloc.dart';

part 'password_form_event.dart';
part 'password_form_state.dart';

class PasswordFormBloc extends Bloc<PasswordFormEvent, PasswordFormState> {
  PasswordFormBloc() : super(StateSuccesTextFieldPassword(password: "")) {
    on<PasswordFormEvent>((event, emit)async {
      if(event is EventFieldPasswordEvent){
        emit(StateSuccesTextFieldPassword(password: event.password));
      }
    });
  }
}
