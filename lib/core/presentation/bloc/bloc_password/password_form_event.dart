part of 'password_form_bloc.dart';

abstract class PasswordFormEvent {}

class EventFieldPasswordEvent extends PasswordFormEvent{
  String? password;
  EventFieldPasswordEvent({this.password});
}
