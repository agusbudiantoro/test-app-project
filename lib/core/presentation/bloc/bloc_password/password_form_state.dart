part of 'password_form_bloc.dart';

abstract class PasswordFormState {}

class PasswordFormInitial extends PasswordFormState {}

class StateSuccesTextFieldPassword extends PasswordFormState {
  String? password;
  StateSuccesTextFieldPassword({this.password});
}
