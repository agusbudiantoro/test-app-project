import 'package:appyou/core/styles/fonts.dart';
import 'package:appyou/core/utils/colors.dart';
import 'package:flutter/material.dart';
typedef ButtonCallBack = void Function(String);
class AppbarCustomGeneral extends StatelessWidget {
  const AppbarCustomGeneral({Key? key, this.clickCallBack, this.name}) : super(key: key);
  final ButtonCallBack? clickCallBack;
  final String? name;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      color: Colors.transparent,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.transparent,
              child: GestureDetector(
                onTap: (){
                  clickCallBack!("success");
                },
                child: Row(
                  children: const [
                    Expanded(
                      flex: 1,
                      child: Icon(Icons.arrow_back_ios_new, size: 14,color: MyColors.fontTitle),
                    ),
                    Expanded(
                      flex: 4,
                      child: Text(
                        "Back",
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            wordSpacing: 0.5,
                            color: MyColors.fontTitle,
                            fontFamily: 'Inter'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.transparent,
              child: Center(
                child: Text(
                  name!,
                  style: customTextStyle(weight: FontWeight.w600, size: 14),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.centerRight,
              child: IconButton(
                onPressed: (){

                },
                color: Colors.white,
                icon: const Icon(Icons.more_horiz),
              ),
            ),
          )
        ],
      ),
    );
  }
}
